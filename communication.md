# Communication

This document describes how the Haskell Foundation Board communicates
among itself and to the broader Haskell community.

## Asynchronous communication

1. Communication within the HF is via GNU Mailman [mailing lists](https://mail.haskell.org/mailman/listinfo). (TODO: Move these lists from `haskell.org` to `haskell.foundation`.)
   The main mailing list of the Board is `board@lists.haskell.foundation`; other
   sub-groups have their own lists, as viewable at the link above. Long-running
   Task Forces and Committees will get their own mailing lists. All archives of
   these lists are public, and the public may write emails to the various groups
   by writing to the mailing list addresses directly; such emails will be moderated,
   but relevant ones (as determined by a list moderator) will be delivered.

   Certain sensitive topics (e.g. personnel) are communicated by direct email
   to the Board members; these emails are not archived.

1. The [Haskell Discourse instance](https://discourse.haskell.org/) maintains
   a [Haskell Foundation category](https://discourse.haskell.org/c/haskell-foundation/11)
   for use in discussing HF matters, for example, to get early feedback on a proposal
   idea. The HF will be sure to monitor this channel of communication.

1. TODO: How will the Haskell Foundation make public announcements?

1. TODO: How will the Haskell Foundation advertise e.g. new board positions? Should
   we have a (non-exhaustive) list of social media websites, etc., to contact?

## Synchronous communication (meetings)

### General Terms

1. Meetings are generally called by the Chair. It is expected that the Chair
   be responsive to a request from another Member to call a meeting, but if
   the Chair is unresponsive, any Member can call a meeting.

2. Meetings are scheduled in a manner as equitable as possible to our varied
   membership. Specifically, meetings will not be scheduled in such a way that
   any one Member is regularly prevented from participating in all meetings.

3. All Board meetings and official Task Force or Committee meetings are
   posted on the official Haskell Foundation calendar. The public
   may access this [calendar](https://calendar.google.com/calendar/u/1?cid=Mmc5OXI0azIxam02aXNyZmQyNmhpdTQ0cWNAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ).

### Agendas

1. All meetings have an agenda posted beforehand by the Member who called
   the meeting (generally the Chair), ideally at least 48 hours before the
   meeting time.

2. Agendas are posted in the appropriate folder in Google Drive.

3. Agendas may be edited in advance of the meeting to add items or clarifying questions.

4. Board Members will not remove others' agenda items or otherwise subvert agendas.

5. After the meeting, the agenda is be made public by being added to the
   [minutes repository](https://gitlab.haskell.org/hf/minutes).

### Minutes

See also our [Transparency](transparency.md) notes.

1. Minutes are taken for all official meetings.

2. For Task Force meetings, the Leader of the Task Force acts as its Secretary.

3. The Secretary creates the minutes document in Google Drive before the meeting.

4. All meeting participants are expected to collaboratively contribute to the
   meeting minutes. The Secretary is ultimately responsible for the accurate
   capturing of the meeting minutes.

5. Attendees of the minutes edit the minutes within 24 hours of the meeting
   to correct any misquotes, etc., that were recorded.

5. The Secretary is responsible to review the minutes after the meeting to
   ensure they are appropriate for publishing.

8. Minutes are automatically ratified 24 hours after the end of the meeting or
   as soon as all pending objections have been resolved, whichever comes
   first.

8. There is the possibility that some details are redacted in the minutes
   before posting. These details might concern, for example, personnel decisions
   or not-yet-closed fund-raising negotations.

9. The Secretary is responsible for publishing the minutes in a timely manner
   after ratification.

1. Minutes are posted in the [hf/minutes](https://gitlab.haskell.org/hf/minutes)
   repository.

## Storage

1. The Haskell Foundation maintains a [Google Drive
   folder](https://drive.google.com/drive/folders/1gxc2miCWM0gwMoA7ywUH3qY--q277Ifc?usp=sharing)
   and a [GitLab group](https://gitlab.haskell.org/hf). Google Drive is good
   for co-editing documents; GitLab is good for long-term archiving and public
   release.

2. All Board members and the Executive Team have read/write access to both the
   Drive folder and the GitLab group. The Chair, the Treasurer, and the Executive
   Director have the ability to add/remove additional individuals.

3. Sub-parts of both the Drive folder and the GitLab group may be made
   public by a decision of the Board.

4. It may be convenient to grant individuals who are not on the Board and not
   employed by the Foundation full access to these resources; any such
   decision will be documented with a rationale.

### Current status

The GitLab group currently hosts three repositories:

* `meta`: Details of how the Haskell Foundation conducts its business. World-readable.

* `minutes`: Ratified minutes of official meetings. World-readable.

* `proposals`: Accepted proposals for what the Haskell Foundation should seek to do.
  Merge Requests in this repository are pending proposals. World-readable.
