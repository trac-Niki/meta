# Transparency

This document sets out the transparency principles and practices of the
Haskell Foundation (HF).

## Principles

1. The Haskell Foundation is a servant of the greater Haskell community; accordingly,
   it must report its work to the greater Haskell community.

2. The Haskell Foundation's success depends on working well with community members;
   accordingly, the public must be able to learn what the HF is
   doing, so as not to make redundant effort.

3. The Haskell Foundation depends on the trust of the community; accordingly, the
   public must be able to audit what the Haskell Foundation has done, to ensure
   this trust is warranted.

4. The Haskell Foundation is fed with ideas from the community; accordingly, there
   must be a way for the community to engage with the HF.

5. The Haskell Foundation is tasked with supporting the Haskell ecosystem
   and infrasturcture; accordingly, the goal of transparency must not supersede
   its other goals.

## Practices

1. Minutes for all official meetings are posted publicly; details are in
   the [communications](communications.md) document.

2. The Haskell Foundation [calendar](https://calendar.google.com/calendar/u/1?cid=Mmc5OXI0azIxam02aXNyZmQyNmhpdTQ0cWNAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ) is public.

3. At least once every two months, there is a public meeting of the Haskell
   Foundation. These are posted on the HF calendar and advertised online. The
   goal of these meetings is to make HF Board members and employees available
   to the public to provide an update on the HF, to answer questions, and
   to source ideas from the community.

   In a nod toward efficiency, other meetings of the Board are not open to the public.

4. Communication within the Haskell Foundation Board is publicly archived; details
   are in the [communication](communication.md) document.

5. The Haskell Foundation maintains a [proposals](https://gitlab.haskell.org/hf/proposals)
   repository, where anyone can submit a proposal for an idea that the HF should
   pursue. This reposotory is the one official way to request that the HF take
   a specific action.

6. A public forum (currently, Discourse) is provided for HF-related discussion.
   Details are in the [communication](communication.md) document.

7. A yearly "State of the Haskell Foundation" report is compiled, posted on
   the [Haskell Foundation website](https://haskell.foundation/). TODO: When
   in the year will this be done?

