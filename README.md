# Haskell Foundation Ways of Working

This "meta" repo contains notes about how the Haskell Foundation
conducts its business. If you want to change the way the Haskell
Foundation does its business, please make an MR or file an Issue
against this repo.

This repo contains (at least) the following documents, included here
in a suggested reading order:

* The [vision document](vision.md) of the Haskell Foundation.

* [Code of Conduct](conduct.md), which sets out the Guidelines for Respectful
  Communication, to which all Board members strive to adhere.

* [Who we are](people.md).

* Our [transparency](transparency.md) principles and practices.

* How we [communicate](communication.md) among ourselves and to the world.


