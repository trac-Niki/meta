# Haskell Foundation vision

The Haskell Foundation (HF) is an independent, non-profit organization
dedicated to broadening the adoption of Haskell, by supporting its ecosystem
of tools, libraries, education, and research.

Why do we need yet another Haskell community organisation? The HF addresses
the following needs, which were previously under-served:

* Driving adoption. HF seeks to foster an understanding of the benefits of
  using Haskell among developers who do not currently use the language, to
  erase barriers to entry, and to improve the Haskell ecosystem. We have many
  passionate and expert groups, but each is focused around a particular piece
  of the ecosystem, and none is dedicated to the overall user experience.

* Technical infrastructure and glue. The HF identifies and integrates or
  implements missing “technical glue”: the useful stuff that makes for a smooth
  user experience (pain-free installers, documentation, error messages, and
  much more).

* Community glue. Our individual groups function well, but on their own lack mechanisms
  for high-quality technical communication between them. The HF tries to
  nurture respectful, inclusive communication across the community.

* Resources and funding. We have plenty of volunteer groups, but as Haskell
  becomes more widely adopted, and more mission critical to more organisations
  it becomes harder for volunteers to sustain all the necessary
  infrastructure. We need colleagues whose day job is to make everything
  work...and that needs funding. Because of its broad scope, the Foundation
  is a more credible recipient of corporate funding than any other
  individual group is.

## Principles and Ethos

How we pursue the goals of HF is just as important what the goals are. HF’s
actions are guided by these core principles:

* **Open source.** Haskell is an open source community and HF embraces the
  open-source ethos wholeheartedly. HF may develop or sponsor the
  development of tools and infrastructure, but it will all be open source.

* **Empowering the community.** A major goal of HF is to augment, celebrate,
  and coordinate the contributions and leadership of volunteers, not to
  supplant or replace them.

* **Open, friendly, and diverse.** For many of us Haskell is more a way of
  life than a programming language. All are welcome; all can contribute.

* **Transparent.** All communication related to code and decision making is
  be publicly accessible, to enable asynchronous communication and
  collaboration. Only certain categories of sensitive information (e.g.
  financial, and matters concerning particular individuals) are kept
  confidential. Details are in the [transparency document](transparency.md).

* **True to Haskell’s principles.** Haskell’s design puts principle ahead of
  expediency, notably by cleaving closely to the principles of purely
  functional programming. Success, yes, but not at all costs!

## Organization

The Haskell community comprises an amazing group of technical talent, and
functions as an almost entirely volunteer effort. The Haskell Foundation
does not change that. Our goal is to make every member of the Haskell
community, and every Haskell committee, feel more supported, and more
productive. We want to enlarge and diversify our community going forward.

More specifically:

* HF has a Board of Directors (“Board” hereafter) that reflects the Haskell
  community and its stakeholders, including academics, commercial users, and
  individuals.

* HF has a full-time staff. The size of the staff varies, depending on funding, but we 
  always have at least an Executive Director (ED), who can organize Haskell outreach,
  support its funding activities and oversee the rest of the staff. The staff
  mostly focuses on funding, marketing, and key infrastructure.

* HF works with existing and new open-source teams to channel energy into
  various efforts like packaging, tools, libraries, compilers, languages,
  documentation, user experience, and infrastructure. To reiterate, we expect
  that most technical contributions will be by volunteers, as it always has been,
  but we position HF to fill gaps that can help adoption.

* HF establishes a [code of conduct](conduct.md) and transparent decision-making that
  applies to itself and any associated teams.

* To the extent HF funds and pursues technical goals itself, it pursues
  those goals with the same transparency as we expect from any of the teams
  associated with it. In this way HF augments the community in a
  transparent way.

## Funding

There have been other initiatives of this kind in the past, but they have
proved hard to sustain. A big part of this has been a simple lack of bandwidth
in a highly decentralised community run entirely by volunteers.

The Haskell Foundation launched with a small group of founding sponsors,
enough to employ an Executive Director. Our goal is to raise around $1M/year
in cash and in-kind contributions.

## Improving the Haskell Adoption Story

A principal goal of the Foundation is to promote adoption of Haskell. We see
that task as having three major components:

1. Eliminating unreasonable and perceived barriers to adoption.

2. Educating the tech community about the benefits of adoption, including
   decision makers.

3. Enhancing the tooling, so that the risk associated with adoption is
   dramatically reduced.

### Eliminating unreasonable barriers to adoption

We expect that with HF adding a little structure, some things can be improved
immediately. For example, we need an entry point for Haskell that speaks to
the needs of a range of users: from engineers looking for an easy on-ramp with
our best advice on how to learn and use Haskell, to team leaders who want to
assess Haskell adoption as a business decision. The Haskell community today
does not provide the full range of content needed to promote adoption. This is
one example where an organization with the right focus and some resources can
easily have a positive impact.

### Educating the tech community

Engineers are typically the ones who drive Haskell adoption. But they often
need permission or sponsorship from managers 1 or 2 levels higher. These
decision makers are people who are more concerned about speed of development,
reliability, maintenance, and people. We need to explicitly address a broader
audience and position Haskell as the best solution to many problems, while
maintaining integrity and avoiding too much “marketing speak".

Conditions for telling this story are favorable. The days of a senior sysadmin
dictating which version of Java or Python “shall be used” are dying.
Containers and cloud technology have inadvertently conspired to permit
engineering teams to make these decisions more often at a team level, even in
larger organizations. Still, these teams need air cover and solid tools so
that their decisions don’t look “rogue” to the rest of the organization.

If used correctly, Haskell can be unreasonably effective. Unfortunately, the
“if” clause is not common knowledge. The HF recognizes that many of these
advanced features are precisely the reason why Haskell can be successful in
cases where other languages fall short. We trust that the end users will be
able to make the right trade-offs that work for them. The HF will be there to
help users that need to make these trade-offs by improving documentation and
highlighting success stories. For example, Haskell has a great concurrency
story and things like STM that are dramatically undersold in the marketplace.
Some advanced features can be emphasized and taught.

The spectrum of developers is very broad: Haskell is used by both junior
developers as well as long-time experts. This can cause friction, and some
code may be unreadable even to other Haskell developers. This is unfortunate,
because we believe that Haskell allows us to express ideas in a way that are
clear and elegant. We need to delineate the common knowledge aspect of the
language and tools to elevate more developers. HF should carry this banner.

Beyond that, the pesky problem of the marketplace for Haskellers remains. We
often hear about fear of hiring bottlenecks and we know some engineering
leaders that list this as the number one concern about Haskell. These leaders
need to know one thing. You don’t have to hire a Haskell team: Haskell can
make your team. Your smart Python developers can learn how to make reliable
software and will love you for letting them. There is a role for HF to bridge
the gap between the countless developers who want to learn and write Haskell,
and the decision makers who currently believe it is hard to find these people.

### Technical Agenda: Enhancing the tooling, filling the gaps

Haskell Foundation identifies a list of technical goals that will ease
adoption and improve Haskell use in production. We have established an
[initial
agenda](https://drive.google.com/file/d/1VmiQJTiejajAmpNKBFKME6Chr2m0tH9t/view?usp=sharing)
and are seeking to refine it as we go forward. As HF evolves, we will engage
technical discussion in a transparent way, with input from the community.
